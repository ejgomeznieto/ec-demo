/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.converter;

import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author Usuario
 */
public class WebFeedbackToPersistenceFeedbackConverter implements Converter<com.example.demo.web.model.Feedback, com.example.demo.persistence.model.Feedback> {

    @Override
    public com.example.demo.persistence.model.Feedback convert(com.example.demo.web.model.Feedback s) {
        com.example.demo.persistence.model.Feedback feedback = new com.example.demo.persistence.model.Feedback();
        feedback.setDate(s.getDate());
        feedback.setEmail(s.getEmail());
        feedback.setId(s.getId());
        feedback.setMessage(s.getMessage());
        feedback.setName(s.getName());
        return feedback;
    }
    

    
}
