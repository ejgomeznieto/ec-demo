/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.converter;

import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author Usuario
 */
public class PersistenceFeedbackToWebFeedbackConverter implements Converter<com.example.demo.persistence.model.Feedback, com.example.demo.web.model.Feedback> {

    @Override
    public com.example.demo.web.model.Feedback convert(com.example.demo.persistence.model.Feedback s) {
        com.example.demo.web.model.Feedback feedback = new com.example.demo.web.model.Feedback();
        feedback.setDate(s.getDate());
        feedback.setEmail(s.getEmail());
        feedback.setId(s.getId());
        feedback.setMessage(s.getMessage());
        feedback.setName(s.getName());
        return feedback;
    }
    

    
}
