package com.example.demo;

import com.example.demo.converter.PersistenceFeedbackToWebFeedbackConverter;
import com.example.demo.converter.WebFeedbackToPersistenceFeedbackConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class DemoApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
            SpringApplication.run(DemoApplication.class, args);
	}
        
        @Override
        public void addFormatters(FormatterRegistry registry) {
            registry.addConverter(new WebFeedbackToPersistenceFeedbackConverter());
            registry.addConverter(new PersistenceFeedbackToWebFeedbackConverter());
        }
        
        @Bean
        public MessageSource messageSource() {
            ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
            messageSource.setBasename("classpath:messages");
            messageSource.setDefaultEncoding("UTF-8");
            return messageSource;
        }

}
