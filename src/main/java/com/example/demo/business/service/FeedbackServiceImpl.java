/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.business.service;

import com.example.demo.persistence.dao.FeedbackRepository;
import com.example.demo.persistence.model.Feedback;
import java.util.Date;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Usuario
 */
@Service
@RequiredArgsConstructor
public class FeedbackServiceImpl implements FeedbackService {
    
    private final FeedbackRepository feedbackRepository;
    
    @Override
    public void create(Feedback feedback) {
        feedback.setDate(new Date());
        feedbackRepository.save(feedback);
    }
    
    @Override
    public List<Feedback> findAll() {
        return feedbackRepository.findAll();
    }
    
}
