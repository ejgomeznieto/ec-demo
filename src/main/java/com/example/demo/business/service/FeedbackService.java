/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.business.service;

import com.example.demo.persistence.model.Feedback;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface FeedbackService {

    public void create(Feedback feedback);
    
    public List<Feedback> findAll();
    
}
