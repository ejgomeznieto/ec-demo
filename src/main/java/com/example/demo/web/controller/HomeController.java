/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.web.controller;

import com.example.demo.business.service.FeedbackService;
import com.example.demo.web.model.Feedback;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class HomeController {

    @RequestMapping(path = "", method = RequestMethod.GET)
    public String getAllFeedback(final ModelMap model) {
        return "index";
    }

}
