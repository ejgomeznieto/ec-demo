/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.web.controller;

import com.example.demo.business.service.FeedbackService;
import com.example.demo.web.model.Feedback;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping("/feedback")
@RequiredArgsConstructor
public class FeedbackController {

    private final FeedbackService feedbackService;

    private final ConversionService conversionService;

    @RequestMapping(path = "new", method = RequestMethod.GET)
    public String newFeedbackForm(final Feedback feedback) {
        return "newfeedbackform";
    }

    @RequestMapping(path = "new", method = RequestMethod.POST)
    public String createNewFeedback(@Valid final Feedback feedback, final BindingResult bindingResult, final ModelMap model, final RedirectAttributes redirectAttrs) {

        if (bindingResult.hasErrors()) {
            return "newfeedbackform";
        }
        
        feedbackService.create(conversionService.convert(feedback, com.example.demo.persistence.model.Feedback.class));
        redirectAttrs.addFlashAttribute("success", "Feedback successfully submitted");

        return "redirect:/";
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public String getAllFeedback(final ModelMap model) {

        List<com.example.demo.persistence.model.Feedback> dbcontent = feedbackService.findAll();
        List<com.example.demo.web.model.Feedback> webContent = dbcontent.stream().map(fb -> {
            return conversionService.convert(fb, com.example.demo.web.model.Feedback.class);
        }).collect(Collectors.toList());
        model.addAttribute("feedbackList", webContent);

        return "feedbackList";
    }

}
