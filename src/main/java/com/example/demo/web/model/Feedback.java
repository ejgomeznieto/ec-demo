/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.web.model;

import java.util.Date;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author Usuario
 */
@Data
public class Feedback {
    
    private Long id;

    @Size(max = 50)
    private String name;
    
    @Email
    @Size(max = 50)
    private String email;
    
    @NotNull
    @NotEmpty
    @Size(max = 500)
    private String message;
    
    private Date date;
    
}
