/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.persistence.dao;

import com.example.demo.persistence.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Usuario
 */
public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
    
}
