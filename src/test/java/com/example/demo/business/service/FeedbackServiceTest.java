/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.business.service;

import com.example.demo.business.service.FeedbackService;
import com.example.demo.persistence.dao.FeedbackRepository;
import com.example.demo.persistence.model.Feedback;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 *
 * @author Usuario
 */
@SpringBootTest
public class FeedbackServiceTest {
    
    @MockBean
    FeedbackRepository feedbackRepository;
    
    @Autowired
    FeedbackService feedBackService;
    
    @Test
    public void createTest() {
        Feedback feedback = new Feedback();
        
        feedback.setEmail("username@server.com");
        feedback.setMessage("message");
        feedback.setName("name");
        
        feedBackService.create(feedback);
        
        verify(feedbackRepository, times(1)).save(any());
        
    }
    
    @Test
    public void findAllTest() {
        Feedback feedback = new Feedback();
        
        feedback.setId(1L);
        feedback.setEmail("username@server.com");
        feedback.setMessage("message");
        feedback.setName("name");
        feedback.setDate(new Date());
        
        List<Feedback> mockFindAllReturn = new ArrayList<>(1);
        mockFindAllReturn.add(feedback);
        
        Mockito.when(feedbackRepository.findAll()).thenReturn(mockFindAllReturn);
        
        List<Feedback> findAllResult = feedBackService.findAll();
        
        verify(feedbackRepository, times(1)).findAll();
        Assertions.assertNotNull(findAllResult);
        Assertions.assertTrue(findAllResult.size() == mockFindAllReturn.size());
        Assertions.assertEquals(feedback, findAllResult.get(0));
    }
    
}
