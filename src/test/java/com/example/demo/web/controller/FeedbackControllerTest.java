/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.web.controller;

import com.example.demo.business.service.FeedbackService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Usuario
 */
@ExtendWith(SpringExtension.class)
@WebAppConfiguration()
@WebMvcTest(FeedbackController.class)
public class FeedbackControllerTest {

    @MockBean
    FeedbackService feedbackService;

    @Mock
    ConversionService conversionService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllFeedbackTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/feedback")
                .accept(MediaType.ALL))
                .andExpect(status().isOk());

    }

    @Test
    public void newFeedbackFormTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/feedback/new")
                .accept(MediaType.ALL))
                .andExpect(status().isOk());

    }

    // Check we redirect to home page with success message
    @Test
    public void createNewFeedback_OKResult_Test() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/feedback/new")
                .accept(MediaType.ALL).param("message", "message"))
                .andExpect(status().isFound()).andExpect(redirectedUrl("/")).andExpect(flash().attributeExists("success"));

    }

    // Check we stay on the same page withh errors if we dont provide mandatory fields
    @Test
    public void createNewFeedback_NOKResult_Test() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/feedback/new")
                .accept(MediaType.ALL))
                .andExpect(status().isOk()).andExpect(model().attributeHasFieldErrors("feedback"));

    }

}
